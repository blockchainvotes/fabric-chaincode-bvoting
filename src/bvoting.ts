/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Context, Contract } from 'fabric-contract-api';
import { Vote } from './vote';

export class BVoting extends Contract {

    public async initLedger(ctx: Context) {
        console.info('============= START : Initialize Ledger ===========');
        const votes: Vote[] = [
            {
                userID: "user12346",
                candidateID: 'Candidate1'
            },
            {
                userID: "user12347",
                candidateID: 'Candidate2'
            },
            {
                userID: "user12348",
                candidateID: 'Candidate3'
            },
            {
                userID: "user12349",
                candidateID: 'Candidate1'
            },
            {
                userID: "user12350",
                candidateID: 'Candidate2'
            },
            {
                userID: "user12351",
                candidateID: 'Candidate1'
            },
            {
                userID: "user12352",
                candidateID: 'Candidate1'
            },
            {
                userID: "user12353",
                candidateID: 'Candidate2'
            },
            {
                userID: "user12354",
                candidateID: 'Candidate1'
            },
            {
                userID: "user12355",
                candidateID: 'Candidate1'
            },
        ];

        for (let i = 0; i < votes.length; i++) {
            votes[i].docType = 'vote';
            await ctx.stub.putState('VOTE' + i, Buffer.from(JSON.stringify(votes[i])));
            console.info('Added <--> ', votes[i]);
        }
        console.info('============= END : Initialize Ledger ===========');
    }

    public async queryVote(ctx: Context, voteNumber: string): Promise<string> {
        const voteAsBytes = await ctx.stub.getState(voteNumber); // get the vote from chaincode state
        if (!voteAsBytes || voteAsBytes.length === 0) {
            throw new Error(`${voteNumber} does not exist`);
        }
        console.log(voteAsBytes.toString());
        return voteAsBytes.toString();
    }

    public async createVote(ctx: Context, voteNumber: string, userID: string, candidateID: string) {
        console.info('============= START : Create Vote ===========');

        const vote: Vote = {
            userID,
            docType: 'vote',
            candidateID,
        };

        await ctx.stub.putState(voteNumber, Buffer.from(JSON.stringify(vote)));
        console.info('============= END : Create Vote ===========');
    }

    public async queryAllVotes(ctx: Context): Promise<string> {
        const startKey = 'VOTE0';
        const endKey = 'VOTE999';
        const allResults = [];
        for await (const {key, value} of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            allResults.push({ Key: key, Record: record });
        }
        console.info(allResults);
        return JSON.stringify(allResults);
    }


    public async changeVoteUserID(ctx: Context, voteNumber: string, newOwner: string) {
        console.info('============= START : changeVoteOwner ===========');

        const voteAsBytes = await ctx.stub.getState(voteNumber); // get the vote from chaincode state
        if (!voteAsBytes || voteAsBytes.length === 0) {
            throw new Error(`${voteNumber} does not exist`);
        }
        const vote: Vote = JSON.parse(voteAsBytes.toString());
        vote.userID = newOwner;

        await ctx.stub.putState(voteNumber, Buffer.from(JSON.stringify(vote)));
        console.info('============= END : changeVoteOwner ===========');
    }

}
