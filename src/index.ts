/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { BVoting } from './bvoting';
export { BVoting } from './bvoting';

export const contracts: any[] = [ BVoting ];
