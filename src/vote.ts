export class Vote {
    public docType?: string;
    public userID: string;
    public candidateID: string;
}